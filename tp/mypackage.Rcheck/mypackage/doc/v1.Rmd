---
title: "MyPackage Vignette 1"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette 1}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---
This is an R-Markdown vignette being written from within an R-Markdown file!

This is how to use the functions in this package:

```{r}
library("mypackage")
var1 <- 1
var2 <- 2
add(var1,var2)
minus(var2,var1)
```
